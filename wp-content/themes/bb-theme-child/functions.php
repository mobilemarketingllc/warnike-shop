<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("calendly_popup","https://assets.calendly.com/assets/external/widget.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// Disable Updraft and Wordfence on localhost
// add_action( 'init', function () {
    // if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
        // include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        // $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        // if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );
       
    // }
    // register_shortcodes();
// } );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
// add_filter('widget_text','do_shortcode');


// function register_shortcodes(){
  // $dir=dirname(__FILE__)."/shortcodes";
  // $files=scandir($dir);
  // foreach($files as $k=>$v){
    // $file=$dir."/".$v;
    // if(strstr($file,".php")){
      // $shortcode=substr($v,0,-4);
      // add_shortcode($shortcode,function($attr,$content,$tag){
        // ob_start();
        // include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        // $c=ob_get_clean();
        // return $c;
      // });
    // }
  // }
// }



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_action('get_footer', 'customfooter_function');

function customfooter_function(){
    

		if ( is_singular(array('carpeting','hardwood_catalog','solid_wpc_waterproof','laminate_catalog','tile_catalog','luxury_vinyl_tile') )) {

		
			echo do_shortcode('[fl_builder_insert_layout slug="gw-3-cta-row"]');
			
		}

}


// // Lets you remove the post anchor and display only title
// function my_function( $link_html, $title, $link ) {
//     return $title;
// }
// add_filter( 'uabb_advanced_post_title_link', 'my_function', 10, 3 );

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

// global $wpdb;
//  $checkalready =  $wpdb->query('SELECT  url,  COUNT(url) FROM wp_redirection_items GROUP BY url HAVING COUNT(url) > 1');
// echo 'dfsdfsd';
//  foreach ($checkalready as $row_404) {
//     print_r($row_404);
//  }

// add_filter( 'body_class','my_body_classes' );
// function my_body_classes( $classes ) {
 
//     if ( is_shop() ) {
     
//         $classes[] = 'class-name';
//         $classes[] = 'class-name-two';
         
//     } else{
//         $classes[] = 'class-name one';
//         $classes[] = 'class-name-two';
//     }

//     return $classes;
     
// }

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

function cart_price( $atts ){
     global $woocommerce; ?>
    <a class="" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('Cart View', 'woothemes'); ?>">       
        <?php echo $woocommerce->cart->get_cart_total(); ?>
    </a>

    <?php
}
add_shortcode( 'CART-PRICE', 'cart_price' );


add_action( 'woocommerce_before_shop_loop_item_title', 'add_categoryname_product_loop', 25);
function add_categoryname_product_loop() {
    global $product;
    $product_cats = wp_get_post_terms($product->id, 'product_cat');
    $count = count($product_cats);
    foreach($product_cats as $key => $cat)
    {
        echo 
        '
        <span class="cat-name">'.$cat->name.'</span>';
        if($key < ($count-1))
        {
            echo ' ';
        }
        else
        {
            echo ' ';
        }
    }
}

add_action( 'widgets_init', 'wb_extra_widgets' );
/**
 * Register new Widget area for Product Cat sort dropdown.
 */
function wb_extra_widgets() {
	register_sidebar( array(
		'id'            => 'prod_sort',
		'name'          => __( 'Product Cat Sort', 'themename' ),
		'description'   => __( 'This site below Shop Title', 'themename' ),
		'before_widget'	=> '<div>',
		'after_widget'	=> '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>'
	) );
}

add_action( 'woocommerce_before_shop_loop','wb_prod_sort' ); // Hook it after headline and before loop
/**
 * Position the Product Category Sort dropdown.
 */
function wb_prod_sort() {
	if ( is_active_sidebar( 'prod_sort' ) ) {
		dynamic_sidebar( 'prod_sort' );
	}
}
/**
 * @snippet       Checkbox to display Custom Product Badge Part 1 - WooCommerce
 * @how-to        Get CustomizeWoo.com FREE
 * @source        https://businessbloomer.com/?p=17419
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.5.3
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// First, let's remove related products from their original position
 
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
 
// Second, let's add a new tab
 
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
 
function woo_new_product_tab( $tabs ) {
    
$tabs['related_products'] = array(
   'title'    => __( 'RELATED PRODUCTS', 'woocommerce' ),
   'priority'    => 50,
   'callback'    => 'woo_new_product_tab_content'
);
   return $tabs;
}
 
// Third, let's put the related products inside
 
function woo_new_product_tab_content() {
woocommerce_output_related_products();
}
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['additional_information']['title'] = __( 'PRODUCT INFO' );	// Rename the additional information tab
	return $tabs;

}

add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $Breadcrumb){
    $shop_page_id = wc_get_page_id('shop'); //Get the shop page ID
    if($shop_page_id > 0 && !is_shop()) { //Check we got an ID (shop page is set). Added check for is_shop to prevent Home / Shop / Shop as suggested in comments
        $new_breadcrumb = [
            _x( 'Shop', 'breadcrumb', 'woocommerce' ), //Title
            get_permalink(wc_get_page_id('shop')) // URL
        ];
        array_splice($crumbs, 1, 0, [$new_breadcrumb]); //Insert a new breadcrumb after the 'Home' crumb
    }
    return $crumbs;
}, 10, 2 );