<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

if ( ! $product_attributes ) {
	return;
}

global $product;

?>
<table class="woocommerce-product-attributes shop_attributes">
	<?php foreach ( $product_attributes as $product_attribute_key => $product_attribute ) : ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo esc_attr( $product_attribute_key ); ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo wp_kses_post( $product_attribute['label'] ); ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo wp_kses_post( $product_attribute['value'] ); ?></td>
		</tr>
	<?php endforeach; ?>
	<?php if(get_post_meta( $product->get_id(), 'carpet_construction', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "carpet_construction"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Carpet Construction"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'carpet_construction', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'carpet_fiber', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "carpet_fiber"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Carpet Fiber"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'carpet_fiber', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'carpet_width_ft', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "carpet_width_ft"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Carpet Width_(ft)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'carpet_width_ft', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'collection_name', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "collection_name"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Collection Name"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'collection_name', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'color_family', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "color_family"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Color Family"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'color_family', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'commercial_residential', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "commercial_residential"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Commercial/Residential"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'commercial_residential', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'construction', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "construction"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Construction"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'construction', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'country_of_manufacturer', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "country_of_manufacturer"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Country Of Manufacturer"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'country_of_manufacturer', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'edge_type', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "edge_type"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Edge Type"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'edge_type', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'face_weight_ozsq_ft', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "face_weight_ozsq_ft"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Face Weight (oz./sq. ft.)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'face_weight_ozsq_ft', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'finish_type', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "finish_type"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Finish Type"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'finish_type', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'floor_surface_texture', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "floor_surface_texture"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Floor Surface Texture"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'floor_surface_texture', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'gloss_level', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "gloss_level"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Gloss Level"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'gloss_level', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'installation_location', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "installation_location"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Installation Location"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'installation_location', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'installation_method', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "installation_method"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Installation Method"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'installation_method', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'installation_type', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "installation_type"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Installation Type"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'installation_type', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'manufacturer_warranty', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "manufacturer_warranty"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Manufacturer Warranty"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'manufacturer_warranty', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'pei_rating', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "pei_rating"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "PEI Rating"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'pei_rating', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'pieces_per_carton', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "pieces_per_carton"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Pieces Per Carton"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'pieces_per_carton', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'plank_width_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "plank_width_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Plank Width (in.)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'plank_width_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_length', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_length"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Length"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_length', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_length_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_length_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Length (in.)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_length_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_thickness_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_thickness_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Thickness (in.)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_thickness_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_thickness_mm', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_thickness_mm"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Thickness (mm)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_thickness_mm', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_width', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_width"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Width"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_width', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_width_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_width_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Width (in.)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_width_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'shade', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "shade"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Shade"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'shade', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'sold_as', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "sold_as"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Sold As"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'sold_as', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'style', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "style"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Style"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'style', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'tile_look', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "tile_look"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Tile Look"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'tile_look', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'tile_material_composition', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "tile_material_composition"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Tile Material Composition"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'tile_material_composition', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'tile_shape', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "tile_shape"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Tile Shape"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'tile_shape', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'tile_size', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "tile_size"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Tile Size"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'tile_size', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'transition_trim_type', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "transition_trim_type"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Transition/Trim Type"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'transition_trim_type', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'vinyl_look', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "vinyl_look"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Vinyl Look"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'vinyl_look', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'wear_layer_thickness_mil', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "wear_layer_thickness_mil"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Wear Layer Thickness (mil)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'wear_layer_thickness_mil', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'wood_species', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "wood_species"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Wood Species"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'wood_species', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'sq_ft_per_carton', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "sq_ft_per_carton"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Sq. ft. Per Carton"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'sq_ft_per_carton', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_length_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_length_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Length (in)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_length_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_width_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_width_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Width (in)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_width_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'square_footage_per_carton', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "square_footage_per_carton"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Square Footage per Carton"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'square_footage_per_carton', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'pieces_per_carton', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "pieces_per_carton"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Pieces Per Carton"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'pieces_per_carton', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'face_weight_ozsq_yd', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "face_weight_ozsq_yd"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Face Weight (oz/sq. yd)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'face_weight_ozsq_yd', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_thickness_mm', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_thickness_mm"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Thickness (mm)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_thickness_mm', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'product_length_in', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "product_length_in"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Product Length (in)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'product_length_in', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'coverage_area_sfcase', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "coverage_area_sfcase"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Coverage Area Sfcase"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'coverage_area_sfcase', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'price_per_sqft', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "price_per_sqft"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Price Per Sq. Ft."; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'price_per_sqft', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'discontinued', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "discontinued"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Discontinued"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'discontinued', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'wear_layer_thickness_mil', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "wear_layer_thickness_mil"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Wear Layer Thickness (mil)"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'wear_layer_thickness_mil', true ); ?></td>
		</tr>
	<?php } ?>
	<?php if(get_post_meta( $product->get_id(), 'suggested_grout_line_size', true )){ ?>
		<tr class="woocommerce-product-attributes-item woocommerce-product-attributes-item--<?php echo "suggested_grout_line_size"; ?>">
			<th class="woocommerce-product-attributes-item__label"><?php echo "Suggested Grout Line Size"; ?></th>
			<td class="woocommerce-product-attributes-item__value"><?php echo get_post_meta( $product->get_id(), 'suggested_grout_line_size', true ); ?></td>
		</tr>
	<?php } ?>
</table>
